from flask import Flask
from flask_cors import CORS

# 新建服务
server = Flask("app")

CORS(server, supports_credentials=True)

# 定义路由
@server.route('/')
def index():
    return 'Hello Flask!'

# 定义路由
@server.route('/info')
def get_info():
  
    # 读取文件
    # file = open('info.json','r',encoding='utf-8')
    # data = file.read()
    # file.close()

    # 更高级的方式
    with open('info.json', 'r', encoding='utf-8') as f:
        data = f.read()
    
    return data

# 开启服务
server.run('0.0.0.0', port=8080)