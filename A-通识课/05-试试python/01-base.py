'''
Author: fzf404
Date: 2021-10-21 09:15:24
LastEditTime: 2021-10-21 15:14:29
Description: python 入门
'''
import json

# 打开文件
json_file = open('info.json','r')

# 读取文件为字符串
json_data = json_file.read()
# 字符串转字典
json_dict = json.loads(json_data)
# 字典取值
json_dict["name"]

json_file.close()