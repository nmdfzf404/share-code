'''
Author: fzf404
Date: 2021-10-21 16:09:28
LastEditTime: 2021-10-21 16:13:29
Description: description
'''
from flask import Flask, request

# 新建服务
server = Flask("app")

# 发送的数据
data = {
    "name": "王山而",
    "sex": 0,
    "intro": "大一学生，就读于沈阳理工大学物联网专业。",
    "about": "身体健康，大脑健全，心态良好，反诈骗能力高。啥都不会，进去想学东西的，emmmm没了。"
}
# 第二种方式
json_file = open('info.json','r')
data = json_file.read()


# 定义路由
@server.route('/')
def index():
    return 'Hello Flask!'

# 定义路由
@server.route('/info')
def get_info():
    return data

# 开启服务
server.run('127.0.0.1', port=8080)

data_raw = csv.reader(open(data_path, 'r'))