'''
Author: fzf404
Date: 2021-10-21 17:36:31
LastEditTime: 2021-10-21 17:46:28
Description: description
'''

import csv
from flask import Flask, request

server = Flask('app')

data_path = 'data.csv'


@server.route('/info')
def get_info():
  # 获得url中的id
  user_id = request.args.get('id')
  
  data = csv.reader(open(data_path, 'r'))

  for item in data:
    if item[0] == user_id:
      return {
          "name": item[1],
          "sex": item[2],
          "intro": item[3],
          "about": item[4]
      }
  return {
    "name": None,
    "sex": None,
    "intro": None,
    "about": None
  }

# 开启服务
server.run('127.0.0.1', port=8080)