$.getJSON('http://127.0.0.1:8080/info', function (json) {

  $('#name').text(json.name)
  $("#intro").text(json.intro)
  $("#about").text(json.about)

  // 0为男生，1为女生
  if (json.sex) {
    $("#sex").text('👦')
  } else {
    $("#sex").text('👧')
  }
  
});