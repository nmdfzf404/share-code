' 定义一个变量名字叫i，他的值为10
dim i
i=10

' 持续执行下面的代码，直到i不大于0
do while i>0
  msgbox(i) 
  i=i-1
loop