Dim ws, notepad
Set ws = CreateObject("WScript.Shell") '创建WScript.Shell对象
Set notepad = ws.Exec("notepad") '运行记事本
ws.AppActivate notepad.ProcessID '激活记事本


WScript.Sleep 300    ' 延时300ms

ws.SendKeys "you are a pig."   ' 写入

WScript.Sleep 300    ' 延时300ms

ws.SendKeys "^a" ' CTRL+A组合键——全选

WScript.Sleep 300    ' 延时300ms

ws.SendKeys "^c" ' CTRL+C组合键——复制

WScript.Sleep 300    ' 延时300ms

ws.SendKeys "%{F4}" ' 关闭当前的记事本窗体

WScript.Sleep 300

ws.SendKeys "!n" ' 关闭当前的记事本窗体,不保存

WScript.Sleep 300

Set notepad = Nothing ' 释放内存
Set ws = Nothing